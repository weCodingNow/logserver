package main

import (
	"net/http"
	"net/url"
	"time"
	"io/ioutil"
	"fmt"

	"github.com/labstack/echo"

	logger "github.com/weCodingNow/logserver/src/logger"
)

type LogInfo struct {
	ip				string
	url				url.URL
	headers 		http.Header
	queryParams 	url.Values
	requestBody		string
	time			time.Time
}

func extractInfo(c echo.Context) LogInfo {
	bytes, err := ioutil.ReadAll(c.Request().Body)

	if err != nil {
		panic(err)
	}
	
	return LogInfo{
		c.RealIP(),
		*c.Request().URL,
		c.Request().Header,
		c.QueryParams(),
		string(bytes),
		time.Now(),
	}
}

func makeHeadersString(h http.Header) string {
	var str string

	for header, fields := range h {
		str += header + ": "

		for i, f := range fields {
			str += f
			if i != len(fields) - 1 {
				str += ","
			}
		}

		str += "\n"
	}

	return str
}

func (info LogInfo) String() string {
	retStr := fmt.Sprintf(
		"[%s]: %s FROM %s\n",
		info.time.Format(time.RFC822),
		info.url.Path,
		info.ip,
	)

	paramStr := info.queryParams.Encode()

	if len(paramStr) > 0 {
		retStr += "----PARAMS----\n" + paramStr + "\n"
	}

	if len(info.requestBody) > 0 {
		retStr += "----BODY----\n" + info.requestBody + "\n"
	}

	retStr += "----HEADERS----\n" + makeHeadersString(info.headers) + "\n"

	return retStr
}

type handler = func(echo.Context) error;

func MakeLoggingHandler(filename string) handler {
	fw := logger.MakeFileWriter(filename)

	return func(c echo.Context) error {
		fmt.Printf("logged this into %s", filename)

		fw <- extractInfo(c).String()

		return c.String(
			http.StatusOK,
			fmt.Sprintf("Hello, you've been logged to %s!\n", filename),
		)
	}
}
