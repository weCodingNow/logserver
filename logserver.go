package main

import (
	"flag"
	"fmt"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	config "github.com/weCodingNow/logserver/src/config"
)

const (
	defaultConfigFile = "config.yml"
	defaultLogFile = "default.log"
)

var (
	configFile = flag.String(
		"cfg",
		defaultConfigFile,
		"path to server config file")
)

func main() {
	flag.Parse();
	config := config.ParseFile(*configFile);

	e := echo.New();
	e.Use(middleware.Logger());

	e.GET("/favicon.ico", func(c echo.Context) error {
		return c.File("Favicon.ico")
	})

	for i, route := range config.Routes {
		if len(route.Logfile) == 0 {
			route.Logfile = defaultLogFile
		}
		
		fmt.Printf("gonna watch %-25s -> %30s", route.Route, route.Logfile);

		if i != len(config.Routes) - 1 {
			fmt.Printf("\n");
		}

		e.Any(route.Route, MakeLoggingHandler(route.Logfile));
	}

	e.Start(config.AddrString());
}
