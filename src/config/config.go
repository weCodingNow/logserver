package config

import (
	"os"
	"io"

	yaml "gopkg.in/yaml.v3"
)

type Entry interface{}

type RouteInfo struct {
	Route		string
	Logfile		string
}

func (route *RouteInfo) FromMap(m map[string]interface{}) {
	for k, v := range m {
		v := v.(string)

		switch k {
		case "url":		route.Route 	= v
		case "file":	route.Logfile 	= v
		}
	}
}

func EntryToRouteInfo (e Entry) RouteInfo {
	route := RouteInfo {
		"",
		"",
	}

	switch i := e.(type) {
	case string:
		route.Route = i
	case map[string]interface{}:
		route.FromMap(i)
	}

	if (len(route.Route) == 0) {
		panic("no url specified");
	}

	return route
}

type Server struct {
	Host 		string 		`yaml:"host"`
	Port 		string 		`yaml:"port"`
}

type RawConfig struct {
	Server					`yaml:"server"`
	RoutesRaw 	[]Entry		`yaml:"logging"`
}

type Config struct {
	Server
	Routes		[]RouteInfo
}

func (rcfg *RawConfig) Ingest() Config {
	var cfg Config

	cfg.Server = rcfg.Server

	for _, entry := range rcfg.RoutesRaw {
		cfg.Routes = append(
			cfg.Routes,
			EntryToRouteInfo(entry),
		)
	}

	return cfg
}

func (cfg *Config) AddrString() string {
	return cfg.Server.Host + ":" + cfg.Server.Port;
}

func Parse(istream io.Reader) Config {
	var rawCfg RawConfig;

	decoder := yaml.NewDecoder(istream);
	err := decoder.Decode(&rawCfg);

	if err != nil {
		ProcessError(err);
	}

	return rawCfg.Ingest();
}

func ParseFile(filename string) Config {
	f, err := os.Open(filename);

	if err != nil {
		ProcessError(err);
	}

	return Parse(f);
}
