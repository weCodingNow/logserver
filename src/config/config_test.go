package config

import (
	"testing"
	"strings"

	"reflect"
)

const testConfig = `
server:
  host: localhost
  port: 3000

logging:
  - url:  /di/callback
    file: callback.log

  - url:  /si/notification
    file: notification.log
`

func TestReadYml(t *testing.T) {
	got := Parse(strings.NewReader(testConfig));

	expected := Config {
		Server{ "localhost", "3000" },
		[]RouteInfo{
			{ "/di/callback", "callback.log" },
			{ "/si/notification", "notification.log" },
		},
	}

	result := reflect.DeepEqual(got, expected)

	if !result {
		t.Errorf("TestReadYml failed, expected\n%v,\ngot\n%v", expected, got)
	}
}