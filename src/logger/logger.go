package logger

import (
	"os"
)

func openLog(filename string) *os.File {
	f, err := os.OpenFile(
		filename,
		os.O_APPEND|os.O_CREATE|os.O_WRONLY,
		0644,
	)

	if err != nil {
		panic(err)
	}

	return f
}

func fileWriter(filename string, in chan string) {
	f := openLog(filename)
	defer f.Close()

	for msg := range in {
		f.WriteString(msg)
	}
}

func MakeFileWriter(filename string) chan string {
	logChannel := make(chan string)
	go fileWriter(filename, logChannel)

	return logChannel
}
